# Shibboleth Migration to the new CERN SSO

Instructions moved to the [Authorization Service documentation](https://auth.docs.cern.ch/user-documentation/saml/shibboleth-migration/)
